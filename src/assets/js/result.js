// console.log('page result');

import review from '@/components/review.vue'; // user
export default {
	components: {
        [review.name]: review
    },
	data() {
		return {
			score: 0,
			modalReviewShow: false
		};
	},
	created() {
		this.$root.checkLogin();
		this.checkModalReview();
	},
	mounted() {
		this.score = this.$route.query.score;
	},
	beforeDestroy() {
		sessionStorage.removeItem('modalReview');
		sessionStorage.removeItem('errorArr');
	},
	methods: {
		// check
		checkModalReview() {
			if(this.$root.isset(JSON.parse(sessionStorage.getItem('modalReview')))) {
				this.modalReviewShow = JSON.parse(sessionStorage.getItem('modalReview'));
			};
		},
		// to home
		toHome() {
			this.$router.replace('/home');
		},
		// to review
		toReview() {
			this.modalReviewShow = true;
			sessionStorage.setItem('modalReview', JSON.stringify(this.modalReviewShow));
		},
		// close modal
		closeModal() {
			this.modalReviewShow = false;
			sessionStorage.setItem('modalReview', JSON.stringify(this.modalReviewShow));
		}
	}
};
