// console.log('page home');

export default {
	data() {
		return {
			hasAnswer: 0
		};
	},
	created() {
		this.$root.checkLogin();
		this.hasAnswer = this.$root.info.hasanswer;
	},
	methods: {
		// 点击进入
		inter() {
			if(this.hasAnswer < 4) {
				this.$router.replace('/test');
			} else {
				this.$router.replace('/fin');
			};
		}
	}
};
