// console.log('page test');

import modalUser from '@/components/modalUser.vue'; // user
import swiper from '../../../public/vendor/swiper/swiper.min.js';

export default {
	components: {
        [modalUser.name]: modalUser
    },
	data() {
		return {
			time: {
				minutes: 20,
				seconds: 0,
			}, // 答题时间
			index: 0, // 当前slide
			acount: 0, // 累计答题数
			allArr: ['a1', 'a2', 'a3', 'a4', 'a5', 'a6', 'a7', 'a8', 'a9', 'a10', 'a11', 'a12', 'a13', 'a14', 'a15', 'a16', 'a17', 'a18', 'a19', 'a20', 'a21', 'a22', 'a23', 'a24', 'a25', 'a26', 'a27', 'a28','a29', 'a30'], // 题组
			qArr: {},
			modalArr: [],
			answers: [
				'A', 'B', 'B', 'B', 'A', 'A', 'A',
				'A', 'C', 'B', 'C', 'B', 'B', 'A', 'A', 'A',
				'B', 'B', 'A', 'C', 'B', 'A', 'A',
				'A', 'C', 'A', 'C', 'D', 'B', 'B'
			],
			score: 0,
			modalProgressShow: false,
			modalLoadingShow: false,
			canSubmit: 1
		};
	},
	created() {
		// this.$root.checkLogin();
		// this.checkTime();
		// this.checkIndex();
		// this.checkAcount();
		// this.checkQArr();
		// this.checkModalArr();
	},
	mounted() {
		let that = this;
		that.$nextTick(() => {
			that.qaSwiper =  new Swiper('.box-swiper .st', {
				initialSlide: 0,
				// loop: true,
				watchSlidesProgress: true,
				resistanceRatio: 0, // 抵抗率。边缘抵抗力的大小比例。值越小抵抗越大越难将slide拖离边缘，0时完全无法拖离
				runCallbacksOnInit: false, // 初始化slide不是第一个 那么初始化时会触发一次 [Transition/SlideChange] [Start/End] 回调函数，如果不想触发，设置为false
				pagination: '.box-progress .swiper-pagination',
				paginationType: 'progress',
				onInit: (swiper) => {
					// console.log('init');
					let slides = swiper.slides,
						sl = slides.length;
					for(let i = 0; i < sl; i++) {
						let slide = slides.eq(i);
						slide.css('zIndex', 100 - i);
					};
					swiper.update(true);
				},
				onAfterResize: (swiper) => {
					// console.log('resize');
					swiper.update(true);
				},
				onSlideChangeStart: (swiper) => {
					// console.log('slideChangeStart');
				},
				onProgress: function(swiper, progress){
					// console.log(swiper.realIndex);
          		},
				onSetTranslate: function(swiper, translate) {
					// console.log('onSetTranslate');
					let slides = swiper.slides,
						sl = slides.length,
						offset = swiper.width * 1;
					for(let i = 0; i < sl; i++) {
						let slide = slides.eq(i),
						progress = slides[i].progress;
						if(progress <= 0) {
							// 右边slide位移  
						    slide.transform('translate3d(' + (progress) * offset + 'px, 0, 0) scale(' + (1 - Math.abs(progress) / 20) + ')');
							slide.css('opacity', (progress + 2.6));    //最右边slide透明
						};
						if(progress > 0) {
							// 左边slide旋转
						    slide.transform('rotate(' + (-progress) * 20 + 'deg)');   
							slide.css('opacity', 1 - progress);    //左边slide透明
						};
					};
				},
				onSetTransition: function(swiper, transition) {
					// console.log('onSetTransition');
					for(let i = 0; i < swiper.slides.length; i++) {
						let slide = swiper.slides.eq(i);
						slide.transition(transition);
					}
				}
			});
			that.qaSwiper.onResize();
		});

		// that.$nextTick(() => {
		// 	that.qaSwiper =  new Swiper('.box-swiper .qas', {
		// 		noSwiping: true,
		// 		initialSlide: that.index,
		// 		runCallbacksOnInit: false, // 初始化slide不是第一个 那么初始化时会触发一次 [Transition/SlideChange] [Start/End] 回调函数，如果不想触发，设置为false
		// 		pagination: '.box-bar .swiper-pagination',
		// 		paginationType: 'progress',
		// 		prevButton: '.btn-prev',
		// 		nextButton: '.btn-next',
		// 		effect: 'fade',
		// 		fade: {
		// 			crossFade: true
		// 		},
		// 		onInit: (swiper) => {
		// 			that.slideFun();
		// 		},
		// 		onSlideChangeStart: (swiper) => {
		// 			that.index = swiper.realIndex;
		// 			that.slideFun();
		// 			sessionStorage.setItem('index', JSON.stringify(that.index));
		// 		}
		// 	});
		// });
	},
	// watch: {
	// 	minute: {
	// 		handler(newVal) {
	// 			this.Num(newVal);
	// 		}
	// 	},
	// 	second: {
	// 		handler(newVal) {
	// 			this.Num(newVal);
	// 		}
	// 	}
	// },
	// computed: {
	// 	minute() {
	// 		return this.Num(this.time.minutes);
	// 	},
	// 	second() {
	// 		return this.Num(this.time.seconds);
	// 	}
	// },
	// beforeDestroy() {
	// 	clearInterval(this.getTimer);
	// 	sessionStorage.removeItem('time');
	// 	sessionStorage.removeItem('index');
	// 	sessionStorage.removeItem('acount');
	// 	sessionStorage.removeItem('qarr');
	// 	sessionStorage.removeItem('modalarr');
	// },
	methods: {
		// user
		toUser() {
			this.$root.userFun('/test');
		},
		// 时间格式
      	Num: function(n) {
        	return n < 10 ? '0' + n : '' + n;
      	},
		// 缓存时间
		checkTime() {
			if(this.$root.isset(JSON.parse(sessionStorage.getItem('time')))) {
				this.time = JSON.parse(sessionStorage.getItem('time'));
				if(this.time.minutes == 0 && this.time.seconds == 0) {
					this.countScore();
					this.checkAnswer();
				} else {
					this.timeDown();
				};
			} else {
				this.timeDown();
			};
		},
		// 倒计时
		timeDown() {
        	this.getTimer = setInterval(() => {
        		this.time.seconds -= 1;
        		if(this.time.minutes > 0) {
        			if(this.time.seconds < 0) {
	            		this.time.minutes -= 1;
	           			this.time.seconds = 59;
	          		};
        		} else {
        			if(this.time.seconds < 0) {
	           			this.time.seconds = 0;
          				clearInterval(this.getTimer);
          				this.countScore();
          				this.checkAnswer();
	          		};
        		};
        		sessionStorage.setItem('time', JSON.stringify(this.time));
        	}, 1000);
      	},
      	// 缓存当前slide
		checkIndex() {
			if(this.$root.isset(JSON.parse(sessionStorage.getItem('index')))) {
				this.index = JSON.parse(sessionStorage.getItem('index'));
			};
		},
		// 缓存答题数
		checkAcount() {
			if(this.$root.isset(JSON.parse(sessionStorage.getItem('acount')))) {
				this.acount = JSON.parse(sessionStorage.getItem('acount'));
			};
		},
		// random questions
		getRandomArr(arr, count) {
		    let shuffled = arr.slice(0), i = arr.length, min = i - count, temp, index;
		    while(i-- > min) {
		        index = Math.floor((i + 1) * Math.random());
		        temp = shuffled[index];
		        shuffled[index] = shuffled[i];
		        shuffled[i] = temp;
		    };
		    return shuffled.slice(min);
		},
		// 缓存题组
		checkQArr() {
			if(this.$root.isset(JSON.parse(sessionStorage.getItem('qarr')))) {
				this.qArr = JSON.parse(sessionStorage.getItem('qarr'));
				this.fillAnswer();
			} else {
				let randomArr = this.getRandomArr(this.allArr, 20),
					len = randomArr.length;
				for(let i = 0; i < len; i++) {
					this.qArr[randomArr[i]] = '';
				};
				sessionStorage.setItem('qarr', JSON.stringify(this.qArr));
			};
		},
		// fill answer
		fillAnswer() {
			let getInput = setInterval(() => {
				if($('input[type="radio"]').length > 0) {
					clearInterval(getInput);
					for(let i in this.qArr) {
						if(this.qArr[i] != '') {
							let len = $('input[name="' + i + '"]').length;
							for(let j = 0; j < len; j++) {
								if($($('input[name="' + i + '"]')[j]).val() == this.qArr[i]) {
									$($('input[name="' + i + '"]')[j]).prop('checked', true);
								};
							};
						};
					};
				};
			}, 100);
		},
		// 缓存弹窗
		checkModalArr() {
			if(this.$root.isset(JSON.parse(sessionStorage.getItem('modalarr')))) {
				this.modalArr = JSON.parse(sessionStorage.getItem('modalarr'));
			};
		},
		// 统计已经答过的题 记录答案
		answerFun(e) {
			let ac = 0,
				key = $(e.currentTarget).prev().attr('name'),
				value = $(e.currentTarget).prev().val(),
				n = $($(e.currentTarget).parents('.swiper-slide')).index() + 1;
			this.qArr[key] = value;
			for(let i in this.qArr) {
				if(this.qArr[i] != '') {
					ac += 1;
				};
			};
			this.acount = ac;
			if(this.modalArr.indexOf(n) < 0) {
				this.modalArr.push(n);
			};
			sessionStorage.setItem('acount', JSON.stringify(this.acount));
			sessionStorage.setItem('qarr', JSON.stringify(this.qArr));
			sessionStorage.setItem('modalarr', JSON.stringify(this.modalArr));
			this.slideFun();
		},
		// 统计分数
		countScore() {
			let score = 0;
			for(let i in this.qArr) {
				if(this.qArr[i] != '') {
					let n = Number(i.substr(1));
					if(this.qArr[i] == this.answers[(n - 1)]) {
						score += 5;
					};
				};
			};
			this.score = score;
		},
		// check answer
		checkAnswer() {
			setTimeout(() => {
				if(this.$root.info.hasanswer == 0) {
					// 没有答过题
					this.postScore();
				} else {
					// 答过题
					if(this.score >= 90) {
						// >=90分 可以获得兑奖码
						this.postScore();
					} else {
						// 只有高于上一次分数才可提交
						if(this.score > this.$root.info.score) {
							this.postScore();
						} else {
							let code = null;
							this.$router.replace({
								path: `/result?score=${this.score}&code=${code}`
							});
						}
					};
				};
			}, 100);
		},
		// post score 
		postScore() {
			let that = this;
			if(that.canSubmit == 0) {
				return;
			} else {
				that.canSubmit = 0;
				that.modalLoadingShow = true;
				$.ajax({
	            	type: 'POST',
	            	// url: 'https://campaign.uglobal.com.cn/ikea/bio/api/access.php?a=addInfo',
	            	url: './api/access.php?a=addInfo',
	            	// async: false,
	                cache: false,
	                dataType: 'json',
	                data: {
	                	'uid': that.$root.info.uid,
	                	'timestamp': that.$root.info.timestamp,
                    	'key': that.$root.info.key,
	                    'score': that.score
	                },
	                success: function(data) {
	                    // console.log(data);
	                    if(data.status == 200) {
	                    	that.$root.info = data.data;
	                    	sessionStorage.setItem('info', JSON.stringify(data.data));
	                    	let code = null;
	                    	if(data.data.score >= 90) {
	                    		if(that.$root.isset(data.data.code3)) {
		                    		code = data.data.code3;
		                    	} else {
		                    		if(that.$root.isset(data.data.code2)) {
		                    			code = data.data.code2;
		                    		} else {
		                    			if(that.$root.isset(data.data.code1)) {
	                    					code = data.data.code1;
		                    			};
		                    		};
		                    	};
	                    	};
	                    	that.$router.replace({
								path: `/result?score=${that.score}&code=${code}`
							});
	                    } else {
	                    	alert(data.info);
	                    };
	                },
	                error: function() {
	                    //
	                    alert('网络错误，请刷新重试');
	                },
	                complete: function() {
	                    //
	                    that.canSubmit = 1;
	                    that.modalLoadingShow = false;
	                }
	            });
			};
		},
		// slide fun
		slideFun() {
			if(this.index < 19) {
				$('.btn-next').prop('disabled', false);
				$('.btn-next').removeClass('swiper-button-disabled');
			} else {
				if(this.acount == Object.keys(this.qArr).length) {
					$('.btn-next').prop('disabled', false);
					$('.btn-next').removeClass('swiper-button-disabled');
				} else {
					$('.btn-next').prop('disabled', true);
					$('.btn-next').addClass('swiper-button-disabled');
				};
			};
		},
		// next
		toEnd() {
			if(this.index == 19 && this.acount == Object.keys(this.qArr).length) {
				this.countScore();
				this.checkAnswer();
			};
		},
		// slide to
		toSlide(n) {
			this.qaSwiper.slideTo(n, 1000, false);
			// this.index = n;
			// this.slideFun();
			// sessionStorage.setItem('index', JSON.stringify(this.index));
		},
		// modal progress
		modalProgress() {
			this.modalProgressShow = true;
		},
		// close modal
		closeModal() {
			this.modalProgressShow = false;
		}
	}
};
