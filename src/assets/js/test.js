// console.log('page test');

import pt from '@/components/progress.vue'; // progress
import swiper from '../../../public/vendor/swiper/swiper.min.js';
import md5 from '../../../public/vendor/md5.min.js';

export default {
	components: {
        [pt.name]: pt
    },
	data() {
		return {
			allArr: ['a1', 'a2', 'a3', 'a4', 'a5', 'a6', 'a7', 'a8', 'a9', 'a10', 'a11', 'a12', 'a13', 'a14', 'a15', 'a16', 'a17', 'a18', 'a19', 'a20', 'a21', 'a22', 'a23', 'a24', 'a25', 'a26', 'a27', 'a28','a29', 'a30', 'a31', 'a32', 'a33', 'a34', 'a35', 'a36', 'a37', 'a38', 'a39', 'a40', 'a41', 'a42', 'a43', 'a44', 'a45', 'a46', 'a47', 'a48', 'a49', 'a50'], // 题组
			answers: [
				'C', 'A', 'C', 'B', 'A', 'A', 'C', 'C','B', 'A', 
				'B', 'C', 'C', 'A', 'B', 'C', 'A','C', 'A', 'B', 
				'B', 'C', 'C', 'B','A', 'C', 'C', 'A', 'B', 'B',
				'A', 'A', 'C', 'B', 'A', 'B', 'C', 'A', 'C', 'A',
				'A', 'B', 'A', 'B', 'A', 'B', 'B', 'A', 'B', 'A'
			],
			time: {
				minutes: 20,
				seconds: 0
			}, // 答题时间
			getTimer: null,
			index: 0, // 当前slide
			acount: 0, // 累计答题数
			qArr: {}, // 随机选出的题目
			modalArr: [], // 进度弹窗
			errorArr: {}, // 错题集
			score: 0,
			modalProgressShow: false,
			modalLoadingShow: false,
			canSubmit: 1
		};
	},
	created() {
		this.$root.checkLogin();
		this.checkTime();
		this.checkIndex();
		this.checkAcount();
		this.checkQArr();
		this.checkModalArr();
	},
	mounted() {
		let that = this;
		that.$nextTick(() => {
			that.qaSwiper =  new Swiper('.box-swiper .qas', {
				initialSlide: that.index,
				resistanceRatio: 0,
				prevButton: '.btn-prev',
				// nextButton: '.btn-next',
				effect: 'coverflow',
				coverflow: {
					rotate: 0,
		            stretch: 10,
		            depth: 100,
		            modifier: 2,
		            slideShadows: false
				},
				onInit: (swiper) => {
					that.slideFun();
				},
				onTransitionStart: (swiper) => {
					if(swiper.realIndex != that.index) {
						that.index = swiper.realIndex;
						that.slideFun();
						sessionStorage.setItem('index', JSON.stringify(that.index));
					};
				}
			});
		});
	},
	watch: {
		minute: {
			handler(newVal) {
				this.Num(newVal);
			}
		},
		second: {
			handler(newVal) {
				this.Num(newVal);
			}
		}
	},
	computed: {
		minute() {
			return this.Num(this.time.minutes);
		},
		second() {
			return this.Num(this.time.seconds);
		}
	},
	beforeDestroy() {
		clearInterval(this.getTimer);
		sessionStorage.removeItem('time');
		sessionStorage.removeItem('index');
		sessionStorage.removeItem('acount');
		sessionStorage.removeItem('qarr');
		sessionStorage.removeItem('modalarr');
	},
	methods: {
		// 时间格式
      	Num: function(n) {
        	return n < 10 ? '0' + n : '' + n;
      	},
		// 缓存时间
		checkTime() {
			if(this.$root.isset(JSON.parse(sessionStorage.getItem('time')))) {
				this.time = JSON.parse(sessionStorage.getItem('time'));
				if(this.time.minutes == 0 && this.time.seconds == 0) {
					this.countScore();
				} else {
					this.timeDown();
				};
			} else {
				this.timeDown();
			};
		},
		// 倒计时
		timeDown() {
        	this.getTimer = setInterval(() => {
        		this.time.seconds -= 1;
        		if(this.time.minutes > 0) {
        			if(this.time.seconds < 0) {
	            		this.time.minutes -= 1;
	           			this.time.seconds = 59;
	          		};
        		} else {
        			if(this.time.seconds < 0) {
	           			this.time.seconds = 0;
          				clearInterval(this.getTimer);
          				this.countScore();
	          		};
        		};
        		sessionStorage.setItem('time', JSON.stringify(this.time));
        	}, 1000);
      	},
      	// 缓存当前slide
		checkIndex() {
			if(this.$root.isset(JSON.parse(sessionStorage.getItem('index')))) {
				this.index = JSON.parse(sessionStorage.getItem('index'));
			};
		},
		// 缓存答题数
		checkAcount() {
			if(this.$root.isset(JSON.parse(sessionStorage.getItem('acount')))) {
				this.acount = JSON.parse(sessionStorage.getItem('acount'));
			};
		},
		// random questions
		getRandomArr(arr, count) {
		    let shuffled = arr.slice(0), i = arr.length, min = i - count, temp, index;
		    while(i-- > min) {
		        index = Math.floor((i + 1) * Math.random());
		        temp = shuffled[index];
		        shuffled[index] = shuffled[i];
		        shuffled[i] = temp;
		    };
		    return shuffled.slice(min);
		},
		// 缓存题组
		checkQArr() {
			if(this.$root.isset(JSON.parse(sessionStorage.getItem('qarr')))) {
				this.qArr = JSON.parse(sessionStorage.getItem('qarr'));
				this.fillAnswer();
			} else {
				let randomArr = this.getRandomArr(this.allArr, 20),
					len = randomArr.length;
				for(let i = 0; i < len; i++) {
					this.qArr[randomArr[i]] = '';
				};
				sessionStorage.setItem('qarr', JSON.stringify(this.qArr));
			};
		},
		// fill answer
		fillAnswer() {
			let getInput = setInterval(() => {
				if($('input[type="radio"]').length > 0) {
					clearInterval(getInput);
					for(let i in this.qArr) {
						if(this.qArr[i] != '') {
							let len = $('input[name="' + i + '"]').length;
							for(let j = 0; j < len; j++) {
								if($($('input[name="' + i + '"]')[j]).val() == this.qArr[i]) {
									$($('input[name="' + i + '"]')[j]).prop('checked', true);
								};
							};
						};
					};
				};
			}, 100);
		},
		// 缓存弹窗
		checkModalArr() {
			if(this.$root.isset(JSON.parse(sessionStorage.getItem('modalarr')))) {
				this.modalArr = JSON.parse(sessionStorage.getItem('modalarr'));
			};
		},
		// 统计已经答过的题 记录答案
		answerFun(e) {
			let ac = 0,
				key = $(e.currentTarget).prev().attr('name'),
				value = $(e.currentTarget).prev().val(),
				n = $($(e.currentTarget).parents('.swiper-slide')).index() + 1;
			this.qArr[key] = value;
			for(let i in this.qArr) {
				if(this.qArr[i] != '') {
					ac += 1;
				};
			};
			this.acount = ac;
			if(this.modalArr.indexOf(n) < 0) {
				this.modalArr.push(n);
			};
			sessionStorage.setItem('acount', JSON.stringify(this.acount));
			sessionStorage.setItem('qarr', JSON.stringify(this.qArr));
			sessionStorage.setItem('modalarr', JSON.stringify(this.modalArr));
			this.slideFun();
		},
		// slide fun
		slideFun() {
			if(this.index < 19) {
				$('.btn-next').prop('disabled', false);
				$('.btn-next').removeClass('swiper-button-disabled');
			} else {
				if(this.acount == Object.keys(this.qArr).length) {
					$('.btn-next').prop('disabled', false);
					$('.btn-next').removeClass('swiper-button-disabled');
				} else {
					$('.btn-next').prop('disabled', true);
					$('.btn-next').addClass('swiper-button-disabled');
				};
			};
		},
		// next
		toEnd() {
			if(this.index == 19 && this.acount == Object.keys(this.qArr).length) {
				this.countScore();
			} else {
				this.qaSwiper.slideNext();
			};
		},
		// slide to
		toSlide(n) {
			this.qaSwiper.slideTo(n, 1000, false);
			this.index = n;
			this.slideFun();
			sessionStorage.setItem('index', JSON.stringify(this.index));
		},
		// modal progress
		modalProgress() {
			this.modalProgressShow = true;
		},
		// close modal
		closeModal() {
			this.modalProgressShow = false;
		},
		// 统计错题和分数
		countScore() {
			sessionStorage.removeItem('errorArr');
			let score = 0,
				ea = {};
			for(let i in this.qArr) {
				let n = Number(i.substr(1));
				if(this.qArr[i] != '') {
					if(this.qArr[i] == this.answers[(n - 1)]) {
						score += 5;
						ea[n] = true;
					} else {
						ea[n] = false;
					}
				} else {
					ea[n] = false;
				};
			};
			this.score = score;
			Object.keys(ea).map(key => {
				this.errorArr['q' + key] = ea[key];
			});
			sessionStorage.setItem('errorArr', JSON.stringify(this.errorArr));
			this.checkAnswer();
		},
		// check answer
		checkAnswer() {
			setTimeout(() => {
				if(this.$root.info.score < 60) {
					// 没达到60分
					if(this.score <= this.$root.info.score) {
						this.$router.replace({
							path: `/result?score=${this.score}`
						});
					} else {
						this.postScore();
					};
				} else {
					this.postScore();
				};
			}, 50);
		},
		// post score 
		postScore() {
			let that = this;
			if(that.canSubmit == 1) {
				that.canSubmit = 0;
				that.modalLoadingShow = true;
				let sign = md5(that.$root.info.key + that.score + that.$root.info.uid);
				$.ajax({
	            	type: 'POST',
	            	// url: 'http://127.0.0.1/apis/test.php?a=updateInfo',
	            	url: './apis/test.php?a=updateInfo',
	            	// async: false,
	                cache: false,
	                dataType: 'json',
	                data: {
	                	'uid': that.$root.info.uid,
	                	'timestamp': that.$root.info.timestamp,
                    	'key': that.$root.info.key,
	                    'score': that.score,
	                    'signnatrue': sign
	                },
	                success: function(data) {
	                    console.log(data);
	                    if(data.code == 200) {
	                    	that.$root.info = data.data;
	                    	sessionStorage.setItem('info', JSON.stringify(data.data));
	                    	that.$router.replace({
								path: `/result?score=${that.score}`
							});
	                    } else {
	                    	alert(data.msg);
	                    };
	                },
	                error: function() {
	                    alert('网络错误，请刷新重试');
	                },
	                complete: function() {
	                    that.canSubmit = 1;
	                    that.modalLoadingShow = false;
	                }
	            });
			};
		}
	}
};
