// console.log('page fin');

export default {
	data() {
		return {};
	},
	created() {
		this.$root.checkLogin();
	},
	methods: {
		// to home
		toHome() {
			this.$router.replace('/home');
		}
	}
};
