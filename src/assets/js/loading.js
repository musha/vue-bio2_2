// console.log('page loading');

import jQuery from '../../../public/vendor/loader/jquery-1.8.3.min.js' // html5loader
import html5Loader from '../../../public/vendor/loader/jquery.html5Loader.min.js' // html5loader
export default {
	data() {
		return {
			source: [
                // {src: require('../img/common/bg.jpg')}
            ]
		};
	},
	mounted() {
        let that = this,
    		// 预加载
            firstLoadFiles = {
                'files': [
                    // common
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/common/bg-bottom1.png'),
                        'size': 4096
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/common/bg-bottom2.png'),
                        'size': 4096
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/common/bg-bottom3.png'),
                        'size': 5120
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/common/bg-bottom4.png'),
                        'size': 4096
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/common/bg-bottom5.png'),
                        'size': 3072
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/common/bg-qa.png'),
                        'size': 4096
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/common/bg-top.png'),
                        'size': 4096
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/common/btn-retest.png'),
                        'size': 4096
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/common/close.png'),
                        'size': 10240
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/common/green.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/common/logo.png'),
                        'size': 2048
                    },
                    // fin
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/fin/banner.png'),
                        'size': 182272
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/fin/bg-banner.png'),
                        'size': 8192
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/fin/btn-home.png'),
                        'size': 4096
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/fin/tip.png'),
                        'size': 10240
                    },
                    // home
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/home/banner.png'),
                        'size': 180224
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/home/bg-banner.png'),
                        'size': 8192
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/home/bottle.png'),
                        'size': 4096
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/home/btn-inter.png'),
                        'size': 7168
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/home/title.png'),
                        'size': 14336
                    },
                    // result
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/result/banner1.png'),
                        'size': 142336
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/result/banner2.png'),
                        'size': 143360
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/result/bg-bottom1.png'),
                        'size': 5120
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/result/bg-bottom2.png'),
                        'size': 5120
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/result/bg-bottom3.png'),
                        'size': 4096
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/result/bg-top.png'),
                        'size': 5120
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/result/btn-again.png'),
                        'size': 4096
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/result/btn-review.png'),
                        'size': 4096
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/result/shapes.png'),
                        'size': 8192
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/result/text1-1.png'),
                        'size': 6144
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/result/text1-2.png'),
                        'size': 4096
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/result/text1-3.png'),
                        'size': 7168
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/result/text2-1.png'),
                        'size': 7168
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/result/text2-2.png'),
                        'size': 5120
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/result/text2-3.png'),
                        'size': 5120
                    },
                    // review
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/review/banner.png'),
                        'size': 150528
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/review/detail1.png'),
                        'size': 6144
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/review/detail2.png'),
                        'size': 10240
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/review/detail3.png'),
                        'size': 6144
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/review/detail4.png'),
                        'size': 7168
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/review/detail5.png'),
                        'size': 4096
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/review/detail6.png'),
                        'size': 6144
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/review/detail7.png'),
                        'size': 6144
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/review/detail8.png'),
                        'size': 5120
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/review/detail9.png'),
                        'size': 6144
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/review/titlem.png'),
                        'size': 3072
                    },
                    // test
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/arrowl.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/arrowr.png'),
                        'size': 2048
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/banner.png'),
                        'size': 148480
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/btn-progress.png'),
                        'size': 4096
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/molecule1.png'),
                        'size': 5120
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/molecule2.png'),
                        'size': 6144
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/titlem.png'),
                        'size': 4096
                    },
                    {
                        'type': 'IMAGE',
                        'source': require('@/assets/img/test/total.png'),
                        'size': 2048
                    },
                ]
            };

        $.html5Loader({
            filesToLoad: firstLoadFiles,
            onBeforeLoad: function() {},
            onElementLoaded: function(obj, elm) {},
            onUpdate: function(percentage) {
                // console.log(percentage);
                $('.box-progress .bar').animate({width: (percentage + '%')}, 10, () => {
                    $('.percent').html(percentage);
                    if(percentage == 100) {
                        setTimeout(() => {
                            that.$root.checkParams();
                        }, 1000);
                    };
                });
            },
            onComplete: function() {}
        });
	}
};
