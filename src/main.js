import Vue from 'vue'
import App from './App.vue'
import router from './router'

// vendor
import ydui from '../public/vendor/ydui.flexible.js'; // 页面分辨率
// import Vconsole from 'vconsole'; // vconsole
// const vConsole = new Vconsole();
// export default vConsole;

Vue.config.devtools = false;
Vue.config.productionTip = false;

new Vue({
  	router,
  	render: h => h(App),
  	data() {
  		return {
  			info: {},
            canGet: 1
  		}
  	},
    created() {},
  	methods: {
        // get param
        getParameterByName(name) {
            name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
            let regex = new RegExp('[\\?&]' + name + '=([^&#]*)'),
                results = regex.exec(location.search);
            return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
        },
        // is set
        isset(param) {
            if(param != '' && param != null && param != undefined) {
                return true;
            } else {
                return false;
            };
        },
  		// check params
  		checkParams() {
            if(this.isset(this.getParameterByName('uid')) && this.isset(this.getParameterByName('name')) && this.isset(this.getParameterByName('dcode')) && this.isset(this.getParameterByName('dname')) && this.isset(this.getParameterByName('timestamp')) && this.isset(this.getParameterByName('key'))) {
                this.checkUser();
            } else {
                alert('缺少参数');
            };
		},
        // check user
        checkUser() {
            let that = this,
            uid = this.getParameterByName('uid'),
            name = this.getParameterByName('name'),
            dcode = this.getParameterByName('dcode'),
            dname = this.getParameterByName('dname'),
            timestamp = this.getParameterByName('timestamp'),
            key = this.getParameterByName('key');
            if(that.canGet == 1) {
                that.canGet = 0;
                $.ajax({
                    type: 'POST',
                    // url: 'http://127.0.0.1/apis/test.php?a=login',
                    url: './apis/test.php?a=login',
                    // async: false,
                    cache: false,
                    dataType: 'json',
                    data: {
                        'uid': uid,
                        'name': name,
                        'dcode': dcode,
                        'dname': dname,
                        'timestamp': timestamp,
                        'key': key
                    },
                    success: function(data) {
                        console.log(data);
                        if(data.code == 200) {
                            that.info = data.data;
                            sessionStorage.setItem('info', JSON.stringify(data.data));
                            that.$router.replace('/home');
                        } else {
                            // alert(data.msg);
                        };
                    },
                    error: function() {
                        alert('网络错误，请刷新重试');
                    },
                    complete: function() {
                        that.canGet = 1;
                    }
                });
            };
        },
		// check login
		checkLogin() {
			if(this.isset(JSON.parse(sessionStorage.getItem('info')))) {
				this.info = JSON.parse(sessionStorage.getItem('info'));
			} else {
				this.$router.replace('/');
			};
		}
  	}
}).$mount('#app')
